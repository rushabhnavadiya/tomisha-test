import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

class HomeController extends GetxController with
    GetSingleTickerProviderStateMixin {
  late TabController tabController;

  final AutoScrollController _autoScrollController = AutoScrollController(
    viewportBoundaryGetter: () =>
    const Rect.fromLTRB(0, 0, 0, 1000 /*MediaQuery.of(context).padding.bottom*/),
    axis: Axis.vertical,
  );

  AutoScrollController get autoScrollController => _autoScrollController;
   final buttonGlobalKey = GlobalKey(debugLabel: 'button');

   var isToShowRegisterButtonOnTap = false.obs;

  @override
  void onInit() {
    tabController = TabController(length: 3, vsync: this,animationDuration: Duration.zero);
    autoScrollController.addListener(() {
      double index0 =
          ( buttonGlobalKey.currentContext?.size?.height  ?? 0) - 130;
      isToShowRegisterButtonOnTap.value = autoScrollController.position.extentBefore > index0;

    });
    super.onInit();
  }

  @override
  void onClose() {
    tabController.dispose();
    super.onClose();
  }
}
