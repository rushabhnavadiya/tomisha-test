import 'package:flutter/material.dart';

class Ellipse extends StatelessWidget {
  final double size;
  const Ellipse({super.key, required this.size, });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: size,
      height: size,
      decoration: const BoxDecoration(
        color: Color(0xFFF7FAFC),
        shape: BoxShape.circle
      ),
    );
  }
}
