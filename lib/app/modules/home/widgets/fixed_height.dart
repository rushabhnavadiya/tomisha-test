import 'package:denie/core/size_config.dart';
import 'package:denie/core/text_styles.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';


class FixedHeightText extends StatelessWidget {
  final String text;
  final String secondText;
  final TextStyle? style;
  final TextStyle? secondStyle;

  const FixedHeightText({
    Key? key,
    required this.text,
    required this.secondText,
    this.style,
    this.secondStyle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(
        minHeight: Responsive.height(106, context),

      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          SizedBox(
            width: Responsive.width(102, context),
            height: Responsive.height(156, context),
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                text,
                textAlign: TextAlign.center,
                style: style ?? latoRegular130(context),
              ),
            ),
          ),
          Container(
            width: Responsive.width(23, context),
          ),
          Flexible(
            child: Padding(
              padding: Responsive.screenWidth(context) > 1020 || !kIsWeb
      ? Responsive.only(context, bottom: 24.0) : const EdgeInsets.only(bottom: 24),
              child: Text(
                secondText,
                style: secondStyle ?? latoRegular30(context),
                maxLines: 3,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
