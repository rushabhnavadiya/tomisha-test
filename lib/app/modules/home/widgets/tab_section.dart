import 'package:denie/app/modules/home/controllers/home_controller.dart';
import 'package:denie/core/app_colors.dart';
import 'package:denie/core/size_config.dart';
import 'package:flutter/material.dart';


class TabSection extends StatelessWidget {
  final HomeController controller;
  final double? height;
  const TabSection({super.key, required this.controller, this.height});

  @override
  Widget build(BuildContext context) {
    return TabBar(
        controller: controller.tabController,
        indicator: BoxDecoration(
          color: color9,
          borderRadius: BorderRadius.only(
            topLeft: controller.tabController.index == 0
                ? const Radius.circular(12)
                : Radius.zero,
            bottomLeft: controller.tabController.index == 0
                ? const Radius.circular(12)
                : Radius.zero,
            topRight: controller.tabController.index == 2
                ? const Radius.circular(12)
                : Radius.zero,
            bottomRight: controller.tabController.index == 2
                ? const Radius.circular(12)
                : Radius.zero,
          ),
        ),
        splashFactory: NoSplash.splashFactory,
        overlayColor:MaterialStateProperty.all(Colors.transparent),
        splashBorderRadius: BorderRadius.zero,
        padding: Responsive.only(context, left: 20, right: 20),
        labelPadding: EdgeInsets.zero,
        indicatorPadding: const EdgeInsets.only(bottom: 2),
        labelColor: Colors.white,
        dividerColor: Colors.transparent,
        unselectedLabelColor: Colors.black,
        indicatorSize: TabBarIndicatorSize.tab,
        onTap: (value) {
          controller.update();
        },
        tabAlignment: TabAlignment.center,
        isScrollable: true,
        tabs: [
          buildTab(context, 'Arbeitnehmer', true, false),
          buildTab(context, 'Arbeitgeber', false, false),
          buildTab(context, 'Temporärbüro', false, true),
        ],
      );
  }

  Widget buildTab(
      BuildContext context,
      String label,
      bool isFirst,
      bool isLast,
      ) {
    final borderRadius = BorderRadius.only(
      topLeft: isFirst ? const Radius.circular(12) : Radius.zero,
      bottomLeft: isFirst ? const Radius.circular(12) : Radius.zero,
      topRight: isLast ? const Radius.circular(12) : Radius.zero,
      bottomRight: isLast ? const Radius.circular(12) : Radius.zero,
    );

    return Tab(
      child: Container(
        width: Responsive.width(160, context) >= 160 ? Responsive.width(160, context) :160,
        height: height ?? Responsive.height(40, context),
        decoration: BoxDecoration(
          border: Border.all(color: color8),
          borderRadius: borderRadius,
        ),
        alignment: Alignment.center,
        child: Text(label),
      ),
    );
  }
}



