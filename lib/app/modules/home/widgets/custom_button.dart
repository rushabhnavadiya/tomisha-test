
import 'package:denie/core/app_colors.dart';
import 'package:denie/core/size_config.dart';
import 'package:denie/core/text_styles.dart';
import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  final double? width;
  final double? height;
  const CustomButton({super.key, this.width, this.height});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height ?? Responsive.height(40, context),
      width: width,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        gradient: const LinearGradient(
          colors: [
            Color(0xFF319795),
            Color(0xFF3182CE),
          ],
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          stops: [0.0, 1.0],
          // tileMode: TileMode.clamp,
          // transform: GradientRotation(95 * 3.14159 / 180),
        ),
      ),
      alignment: Alignment.center,
      child: Text(
        'Kostenlos Registrieren',
        style: latoSemibold14(context).copyWith(
          color: color5,
          letterSpacing: 0.84
        ),
      ),
    );
  }
}
