import 'package:denie/app/modules/home/controllers/home_controller.dart';
import 'package:denie/core/app_colors.dart';
import 'package:denie/core/responsive.dart';
import 'package:denie/core/size_config.dart';
import 'package:denie/core/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TopWidget extends StatelessWidget {
  final HomeController homeController;
  const TopWidget({super.key, required this.homeController});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: Responsive.height(67, context) < 67 && ResponsiveScreenType.isDesktop(context) ?
      67 : Responsive.height(67, context),
      decoration: const BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: color11,
            offset: Offset(0, 3),
            blurRadius: 6,
          ),
        ],
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(0),
          topRight: Radius.circular(0),
          bottomLeft: Radius.circular(12),
          bottomRight: Radius.circular(12),
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const GradientDivider(),
          Expanded(
            child: Align(
              alignment: Alignment.centerRight,
              child: Obx( () {


                return Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [

                    if(homeController.isToShowRegisterButtonOnTap
                        .value && (ResponsiveScreenType.isDesktop(context)) )...[
                      Text(
                        'Jetzt Klicken',
                        style: latoNormal19(context).copyWith(
                            letterSpacing: 0.84),
                      ),
                      SizedBox(width: Responsive.width(20, context),),
                      Container(
                        width: Responsive.width(255, context),
                        height: Responsive.height(40, context),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(
                              color: const Color(0xFFCBD5E0), width: 1),
                          borderRadius: BorderRadius.circular(12),
                        ),
                        alignment: Alignment.center,
                        child: Text(
                          'Kostenlos Registrieren',
                          style: latoSemibold14(context).copyWith(
                              letterSpacing: 0.84),
                        ),
                      ),
                      SizedBox(width: Responsive.width(20, context),),
                    ],
                    Padding(
                      padding: Responsive.only(context, right: 17),
                      child: Text(
                        'Login',
                        style: latoSemibold14(context).copyWith(
                            letterSpacing: 0.84),
                      ),
                    ),
                  ],
                );
              }),
            ),
          )
        ],
      ),
    );
  }
}

class GradientDivider extends StatelessWidget {
  const GradientDivider({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      height: Responsive.height(5, context),
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          colors: [
            color10,
            color12,
          ],
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          stops: [0.0, 1.0],
          // tileMode: TileMode.clamp,
          // transform: GradientRotation(91 * 3.14159 / 180),
        ),
      ),
    );
  }
}
