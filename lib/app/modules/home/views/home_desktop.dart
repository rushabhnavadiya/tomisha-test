import 'package:denie/app/modules/home/boarding_model.dart';
import 'package:denie/app/modules/home/controllers/home_controller.dart';
import 'package:denie/app/modules/home/widgets/custom_button.dart';
import 'package:denie/app/modules/home/widgets/ellipse.dart';
import 'package:denie/app/modules/home/widgets/fixed_height.dart';
import 'package:denie/app/modules/home/widgets/gradient_divider.dart';
import 'package:denie/app/modules/home/widgets/tab_section.dart';
import 'package:denie/core/constants.dart';
import 'package:denie/core/image_assets.dart';
import 'package:denie/core/size_config.dart';
import 'package:denie/core/text_styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import 'package:flutter_svg_provider/flutter_svg_provider.dart' as fsp;

class HomeDesktop extends StatelessWidget {
  const HomeDesktop({Key? key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (logic) {
        final controller = Get.find<HomeController>();

        return SizedBox(
          height: Responsive.screenHeight(context),
          child: Stack(
            children: [
              NestedScrollView(
                controller: controller.autoScrollController,
                headerSliverBuilder: (context, value) {
                  return [
                    SliverToBoxAdapter(
                        child: headerSection(context, controller)),
                    SliverToBoxAdapter(
                      child: Container(
                        width: double.maxFinite,
                        padding: Responsive.symmetric(
                            context, vertical: 36),
                        child: Container(
                          height: Responsive.height(70, context),
                          padding: Responsive.symmetric(
                              context, vertical: 5),
                          alignment: Alignment.center,
                          child: TabSection(
                            controller: controller,
                            height: Responsive.height(70, context),
                          ),
                        ),
                      ),
                    ),
                  ];
                },
                body: TabBarView(
                    controller: controller.tabController,
                    physics: const NeverScrollableScrollPhysics(),
                    children: List.generate(
                        3,
                            (index) => BodySection(
                          boardData: contents(context)[index],
                          index: index,
                        ))),
              ),
              TopWidget(homeController: controller),
            ],
          ),
        );
      },
    );
  }

  Widget headerSection(BuildContext context, HomeController controller) {
    return Container(
      key: controller.buttonGlobalKey,
      width: double.maxFinite,
      constraints: BoxConstraints(
        minHeight: Responsive.height(659, context) + (Responsive.height(67, context) < 67 ? 67 :Responsive.height(67, context) ),
      ),
      decoration: const BoxDecoration(
        // gradient: LinearGradient(
        //   colors: [
        //     Color(0xFFEBF4FF),
        //     Color(0xFFE6FFFA),
        //   ],
        //   begin: Alignment.topLeft,
        //   end: Alignment.bottomRight,
        //   stops: [0.0, 1.0],
        // ),
          image: DecorationImage(
            image: fsp.Svg(
              b3Svg,
            ),
            fit: BoxFit.fill,

          )
      ),
      padding: Responsive.symmetric(context, horizontal: 374),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                constraints: BoxConstraints(
                  minHeight: Responsive.height(156, context),
                ),
                child: Text(
                  "Deine Job\nwebsite",
                  style:
                  latoMedium65(context).copyWith(letterSpacing: 1.8),
                  textAlign: TextAlign.start,
                ),
              ),
              LayoutBuilder(
                builder: (context, constraints) {
                  double remainingHeight = constraints.maxHeight -
                      Responsive.height(156, context) -
                      Responsive.height(50, context);
                  double middleContainerHeight =
                  remainingHeight > Responsive.height(53, context)
                      ? Responsive.height(53, context)
                      : remainingHeight / 2;
                  return SizedBox(
                    height: middleContainerHeight,
                  );
                },
              ),
              CustomButton(
                width: Responsive.width(320, context),
                height: Responsive.height(50, context),
              )
            ],
          ),
          SizedBox(
            width: Responsive.width(151, context),
          ),
          Container(
            height: Responsive.height(455, context),
            width: Responsive.width(455, context),
            decoration: const BoxDecoration(
                color: Colors.white,
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: fsp.Svg(
                    agreementSvg,
                  ),
                  fit: BoxFit.contain,
                )),
          ),
        ],
      ),
    );
  }
}

class BodySection extends StatelessWidget {
  final ContentData boardData;
  final int index;

  const BodySection({
    Key? key,
    required this.boardData,
    required this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: NeverScrollableScrollPhysics(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            width: double.maxFinite,
            child: Text(
              boardData.mainTitle,
              style: latoMedium21(context),
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            height: Responsive.height(20, context),
          ),
          buildFirstContentWidget(
              context: context,
              imageAsset: boardData.image1,
              text: boardData.text1),
          SizedBox(
            height: Responsive.height(95, context),
          ),
          buildSecondContentWidget(
              context: context,
              imageAsset: boardData.image2,
              text: boardData.text2),
          SizedBox(
            height: Responsive.height(95, context),
          ),
          buildThirdContentWidget(
              context: context,
              imageAsset: boardData.image3,
              text: boardData.text3),
        ],
      ),
    );
  }

  Widget buildFirstContentWidget({
    required BuildContext context,
    required ImageData imageAsset,
    required String text,
  }) {
    return Container(
      width: double.maxFinite,
      height: Responsive.height(265, context),
      padding: Responsive.only(context, left: 343),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            height: Responsive.height(208, context),
            child: Stack(
              children: [
                Ellipse(
                  size: Responsive.height(208, context),
                ),
                Container(
                  width: Responsive.width(522, context),
                  padding: Responsive.only(context, left: 35, bottom: 23),
                  child: FixedHeightText(
                    text: "1.",
                    secondText: text,
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            width: Responsive.width(62, context),
          ),
          SizedBox(
            height: imageAsset.height,
            width: imageAsset.width,
            child: SvgPicture.asset(
              imageAsset.image,
            ),
          )
        ],
      ),
    );
  }

  Widget buildSecondContentWidget({
    required BuildContext context,
    required ImageData imageAsset,
    required String text,
  }) {
    return Container(
      width: double.maxFinite,
      height: Responsive.height(370, context),
      padding: Responsive.only(context, left: 558),
      decoration: BoxDecoration(
        // gradient: LinearGradient(
        //   begin: Alignment.topLeft,
        //   end: Alignment.bottomRight,
        //   colors: [
        //     Color(0xFFE6FFFA),
        //     Color(0xFFEBF4FF),
        //   ],
        //   stops: [0.0, 1.0],
        //   transform: GradientRotation(104 * 3.14159 / 180),
        // ),

          image: DecorationImage(
            image: fsp.Svg(
              b4Svg,
            ),
            fit: BoxFit.fill,

          )
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            height: imageAsset.height,
            width: imageAsset.width,
            child: SvgPicture.asset(
              imageAsset.image,
            ),
          ),
          SizedBox(
            width: Responsive.width(62, context),
          ),
          SizedBox(
            width: Responsive.width(522, context),
            height: Responsive.height(156, context),
            child: FixedHeightText(
              text: "2.",
              secondText: text,
            ),
          ),
        ],
      ),
    );
  }

  Widget buildThirdContentWidget({
    required BuildContext context,
    required ImageData imageAsset,
    required String text,
  }) {
    return Container(
      width: double.maxFinite,
      height: Responsive.height(265, context),
      padding: Responsive.only(context, left: 465),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            height: Responsive.height(310, context),
            width: Responsive.width(522, context),
            child: Stack(
              children: [
                Ellipse(
                  size: Responsive.width(303, context),
                ),
                Positioned(
                  top: 0,
                  child: Container(
                    width: Responsive.width(522, context),
                    padding: Responsive.only(context, left: 110, top: 0,),
                    child: FixedHeightText(
                      text: "3.",
                      secondText: text,
                    ),
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            width: Responsive.width(62, context),
          ),
          SizedBox(
            height: imageAsset.height,
            width: imageAsset.width,
            child: SvgPicture.asset(
              imageAsset.image,
            ),
          )
        ],
      ),
    );
  }
}

/*Row(
                    children: [
                      Container(
                        constraints: BoxConstraints(
                            minHeight: Responsive.height(156, context),
                        ),
                        alignment: Alignment.center,
                        child: Text(
                          '1.',
                          style: latoRegular130(context),
                        ),
                      ),
                      SizedBox(
                        width: Responsive.width(23, context),
                      ),
                      Flexible(
                        child:  Container(
                          constraints: BoxConstraints(
                            minHeight: Responsive.height(156, context),
                          ),
                          // padding:  Responsive.only(context,bottom: 5.0),
                          alignment: Alignment.bottomLeft,
                          child: Text(
                            text,
                            style: latoRegular30(context),
                            maxLines: 3,
                          ),
                        ),
                      )
                    ],
                  )*/
