import 'package:denie/app/modules/home/boarding_model.dart';
import 'package:denie/app/modules/home/controllers/home_controller.dart';
import 'package:denie/app/modules/home/widgets/custom_button.dart';
import 'package:denie/app/modules/home/widgets/ellipse.dart';
import 'package:denie/app/modules/home/widgets/fixed_height.dart';
import 'package:denie/app/modules/home/widgets/gradient_divider.dart';
import 'package:denie/app/modules/home/widgets/tab_section.dart';
import 'package:denie/core/constants.dart';
import 'package:denie/core/image_assets.dart';
import 'package:denie/core/size_config.dart';
import 'package:denie/core/text_styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:flutter_svg_provider/flutter_svg_provider.dart' as fsp;

class HomeMobile extends StatelessWidget {
  const HomeMobile({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (logic) {
        final controller = Get.find<HomeController>();

        return SizedBox(
          height: Responsive.screenHeight(context),
          child: Stack(
            children: [
              NestedScrollView(
                headerSliverBuilder: (context, value) {
                  return [
                    SliverToBoxAdapter(child: headerSection(context)),
                    SliverToBoxAdapter(
                        child: Container(
                      width: double.maxFinite,
                      padding: Responsive.symmetric(context, vertical: 20),
                      child: Container(
                        height: Responsive.height(50, context),
                        padding: Responsive.symmetric(context, vertical: 5),
                        alignment: Alignment.center,
                        child: TabSection(
                          controller: controller,
                        ),
                      ),
                    )),
                  ];
                },
                body: TabBarView(
                    controller: controller.tabController,
                    // physics: NeverScrollableScrollPhysics(),
                    children: List.generate(
                        3, (index) => BodySection(boardData: contents(context)[index],index: index,))),
              ),
              TopWidget(homeController: controller,),

              Align(
                alignment: Alignment.bottomCenter,
                child: bottomWidget(context),
              ),
            ],
          ),
        );
      },
    );
  }

  Widget bottomWidget(BuildContext context) {
    return Container(
      height: Responsive.height(84, context),
      decoration: const BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Color(0x33000000),
            offset: Offset(0, -1),
            blurRadius: 3,
          ),
        ],
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(12),
          topRight: Radius.circular(12),
          bottomLeft: Radius.circular(0),
          bottomRight: Radius.circular(0),
        ),
      ),
      padding: Responsive.symmetric(context, horizontal: 20.2),
      child: const Center(
        child: CustomButton(),
      ),
    );
  }

  Widget headerSection(BuildContext context) {
    return Container(
      width: double.maxFinite,
      constraints: BoxConstraints(minHeight: Responsive.height(659, context),),
      decoration: const BoxDecoration(
        // gradient: LinearGradient(
        //   colors: [
        //     Color(0xFFEBF4FF),
        //     Color(0xFFE6FFFA),
        //   ],
        //   begin: Alignment.topLeft,
        //   end: Alignment.bottomRight,
        //   stops: [0.0, 1.0],
        // ),
          image: DecorationImage(
            image: fsp.Svg(
              b2Svg,
            ),
            fit: BoxFit.fitHeight,

          )
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Flexible(
            child: Container(
              constraints: BoxConstraints(
                minHeight: Responsive.height(113, context),
              ),
              margin: Responsive.only(context, left: 20, right: 20, top: 90),
              child: Text(
                "Deine Job\nwebsite",
                style: latoMedium42(context).
                copyWith(letterSpacing: 1.26,
                    fontWeight: FontWeight.w600,fontSize: 42),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          SizedBox(
            height: Responsive.height(383, context),
            width: Responsive.width(385, context),
            child: SvgPicture.asset(
              agreementSvg,
              alignment: Alignment.topCenter,
              fit: BoxFit.fitHeight,
            ),
          ),
        ],
      ),
    );
  }
}

class BodySection extends StatelessWidget {
  final ContentData boardData;
  final int index;
  const BodySection({
    super.key,
    required this.boardData, required this.index,
  });

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      // physics: NeverScrollableScrollPhysics(),
      child: Wrap(
        // mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            width: double.maxFinite,
            margin: Responsive.symmetric(context, horizontal: 40.0),
            child: Text(
              boardData.mainTitle,
              style: latoMedium21(context),
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            height: Responsive.height(20, context),
          ),
          buildFirstContentWidget(
              context: context,
              imageAsset: boardData.image1,
              text: boardData.text1),
          buildSecondContentWidget(
              context: context,
              imageAsset: boardData.image2,
              text: boardData.text2),
          buildThirdContentWidget(
              context: context,
              imageAsset: boardData.image3,
              text: boardData.text3),
      
      
          Container(
            height: Responsive.height(100, context),
          ),
        ],
      ),
    );
  }

  Widget buildFirstContentWidget({
    required BuildContext context,
    required ImageData imageAsset,
    required String text,
  }) {
    return SizedBox(
      width: double.maxFinite,
      height: Responsive.height(265, context),
      child: Stack(
        children: [
          Positioned(
            bottom: 0,
            left: Responsive.width(-40, context),
            child: Ellipse(
              size: Responsive.height(208, context),
            ),
          ),
          Positioned(
              top: 0,
              right: Responsive.width(25, context),
              child: SvgPicture.asset(
                imageAsset.image,
                height: imageAsset.height,
              )),
          Positioned(
              bottom: Responsive.height(23, context),
              left: Responsive.width(12, context),
              right: Responsive.width(12, context),
              child: FixedHeightText(text: "1.",
              secondText: text,secondStyle:latoRegular1575(context) ,),
              // child: Row(
              //   crossAxisAlignment: CrossAxisAlignment.end,
              //   children: [
              //     SizedBox(
              //       height: Responsive.height(156, context),
              //       child: Text(
              //         '1.',
              //         style: latoRegular130(context),
              //       ),
              //     ),
              //     SizedBox(
              //       width: Responsive.width(23, context),
              //     ),
              //     Flexible(
              //       child: Padding(
              //         padding: Responsive.only(context, bottom: 22.0),
              //         child: Text(
              //           text,
              //           style: latoRegular1575(context),
              //           maxLines: 3,
              //         ),
              //       ),
              //     )
              //   ],
              // )
          ),
        ],
      ),
    );
  }

  Widget buildSecondContentWidget({
    required BuildContext context,
    required ImageData imageAsset,
    required String text,
  }) {
    return Container(
      width: double.maxFinite,
      height: Responsive.height(370, context),
      decoration: const BoxDecoration(
        // gradient: LinearGradient(
        //   begin: Alignment.topLeft,
        //   end: Alignment.bottomRight,
        //   colors: [
        //     Color(0xFFE6FFFA), // Start color of the gradient
        //     Color(0xFFEBF4FF), // End color of the gradient
        //   ],
        //   stops: [0.0, 1.0],
        //   // transform: GradientRotation(134 * 3.14159 / 180), // Rotate gradient
        // ),
          image: DecorationImage(
            image: fsp.Svg(
              bg1Svg,
            ),
            fit: BoxFit.fill,

          )
      ),
      child: Stack(
        children: [
          Positioned(
            bottom: Responsive.height(62, context),
            right: Responsive.width(
                index == 0 ? 67 :
                index == 1 ? 42 :40, context),
            left: Responsive.width(index == 0 ? 112 :
            index == 1 ? 58 :102, context),
            child: SizedBox(
              height:  imageAsset.height,
              width:  imageAsset.width,
              child: SvgPicture.asset(
                imageAsset.image,
              ),
            ),
          ),
          Positioned(
              top: Responsive.height(27, context),
              left: Responsive.width(37, context),
              right: Responsive.width(19, context),
              child:FixedHeightText(text: "2.",
                secondText: text,secondStyle:latoRegular1575(context) ,),
              // child: Row(
              //   crossAxisAlignment: CrossAxisAlignment.end,
              //   children: [
              //     SizedBox(
              //       height: Responsive.height(156, context),
              //       child: Text(
              //         '2.',
              //         style: latoRegular130(context),
              //       ),
              //     ),
              //     SizedBox(
              //       width: Responsive.width(23, context),
              //     ),
              //     Flexible(
              //       child: Padding(
              //         padding: Responsive.only(context, bottom: 22.0),
              //         child: Text(
              //           text,
              //           style: latoRegular1575(context),
              //           maxLines: 3,
              //         ),
              //       ),
              //     )
              //   ],
              // )
          ),
        ],
      ),
    );
  }

  Widget buildThirdContentWidget({
    required BuildContext context,
    required ImageData imageAsset,
    required String text,
  }) {
    return SizedBox(
      width: double.maxFinite,
      height: Responsive.height(index == 0? 327:
      index ==1 ? 370:350, context),
      child: Stack(
        children: [
          Positioned(
            left: Responsive.width(-54, context),
            child: Ellipse(
              size: Responsive.height(305, context),
            ),
          ),
          Positioned(
              bottom: 0,
              right: Responsive.width(
                  index == 0 ? 2 :
                  index == 1 ? 60 :55, context),
              left: index == 0 ? Responsive.width(
                  index == 1 ? 60 :55, context):null,
              child: SizedBox(
                height:  imageAsset.height,
                width:  imageAsset.width,
                child: SvgPicture.asset(
                  imageAsset.image,
                  fit: BoxFit.contain,
                ),
              )),
          Positioned(
              top: -10,
              left: Responsive.width(50, context),
              right: Responsive.width(38, context),

              child:FixedHeightText(text: "3.",
                secondText: text,secondStyle:latoRegular1575(context) ,),
              // child: Row(
              //   crossAxisAlignment: CrossAxisAlignment.end,
              //   children: [
              //     SizedBox(
              //       height: Responsive.height(156, context),
              //       child: Text(
              //         '3.',
              //         style: latoRegular130(context),
              //       ),
              //     ),
              //     SizedBox(
              //       width: Responsive.width(23, context),
              //     ),
              //     Flexible(
              //       child: Padding(
              //         padding: Responsive.only(context, bottom: 22.0),
              //         child: Text(
              //           text,
              //           style: latoRegular1575(context),
              //           maxLines: 3,
              //         ),
              //       ),
              //     )
              //   ],
              // )
          ),
        ],
      ),
    );
  }
}
