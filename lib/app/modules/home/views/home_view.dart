import 'package:denie/app/modules/home/views/home_desktop.dart';
import 'package:denie/app/modules/home/views/home_mobile.dart';
import 'package:denie/core/responsive.dart';
import 'package:flutter/material.dart';



class HomeView extends StatefulWidget {
  const HomeView({super.key});

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
     backgroundColor: Colors.white,
      body:SafeArea(
        child: ResponsiveScreenType(
          mobile: HomeMobile(),
          desktop: const HomeDesktop(),
          tablet: HomeDesktop(),
        ),
      )
    );
  }
}
