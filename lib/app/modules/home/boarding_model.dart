class ContentData {
  final String mainTitle;
  final ImageData image1;
  final String text1;
  final ImageData image2;
  final String text2;
  final ImageData image3;
  final String text3;

  ContentData({
    required this.mainTitle,
    required this.image1,
    required this.text1,
    required this.image2,
    required this.text2,
    required this.image3,
    required this.text3,
  });
}

class ImageData {
  final String image;
  final double height;
  final double width;

  ImageData({
    required this.image,
    required this.height,
    required this.width,
  });
}


