import 'package:denie/core/responsive.dart';
import 'package:flutter/material.dart';

class Responsive {
  // Constants based on Figma design
  static const double figmaDesignWidth = 360.0;
  static const double figmaDesignHeight = 640.0; //
  static const double figmaDesktopDesignWidth = 1920.0;
  static const double figmaDesktopDesignHeight = 1080.0; // Adjust as needed
  static const double figmaSafeAreaHeight = 44.0; // Adjust as needed

  // Get device screen width
  static double screenWidth(BuildContext context) {
    return MediaQuery.of(context).size.width;
  }

  // Get device screen height
  static double screenHeight(BuildContext context) {
    return MediaQuery.of(context).size.height;
  }

  // Get device safe area
  static double screenSafeAreaHeight(BuildContext context) {
    return MediaQuery.of(context).padding.top;
  }

  // Get device screen height
  static double statusBarHeight(BuildContext context) {
    return MediaQuery.of(context).padding.top;
  }

  // Calculate responsive width based on Figma design width
  static double width(double px, BuildContext context) {
    return (px / (ResponsiveScreenType.isDesktop(context) ? figmaDesktopDesignWidth
        :  figmaDesignWidth)) * screenWidth(context);
  }

  // Calculate responsive height based on Figma design height
  static double height(double px, BuildContext context) {
    return (px / (ResponsiveScreenType.isDesktop(context) ? figmaDesktopDesignHeight :
    figmaDesignHeight)) * screenHeight(context);
  }

  // // Calculate responsive font size
  // static double fontSize(double px, BuildContext context) {
  //   return
  //   (px / (ResponsiveScreenType.isDesktop(context) ? figmaDesktopDesignWidth
  //       : figmaDesignWidth)) * screenWidth(context);
  // }


  static double fontSize(double px, BuildContext context) {
    if(ResponsiveScreenType.isDesktop(context)){
      // Determine the screen width and height
      double screenWidth = MediaQuery.of(context).size.width;
      double screenHeight = MediaQuery.of(context).size.height;

      // Calculate the scaling factor based on width and height
      double scaleFactor = screenWidth / figmaDesktopDesignWidth;

      // If height is the limiting factor, adjust the scaling factor
      if (screenHeight * scaleFactor > figmaDesignHeight) {
        scaleFactor = figmaDesignHeight / screenHeight;
      }

      // Calculate the font size based on the scaling factor and input size
      return px * scaleFactor;
    }
      return
      (px / figmaDesignWidth) * screenWidth(context);
  }
  // Calculate responsive padding/margin for all sides
  static EdgeInsetsGeometry all(double px, BuildContext context) {
    final double responsiveValue = width(px, context);
    return EdgeInsets.all(responsiveValue);
  }

  // Calculate responsive horizontal padding/margin
  static EdgeInsetsGeometry horizontal(double px, BuildContext context) {
    final double responsiveValue = width(px, context);
    return EdgeInsets.symmetric(horizontal: responsiveValue);
  }

  // Calculate responsive vertical padding/margin
  static EdgeInsetsGeometry vertical(double px, BuildContext context) {
    final double responsiveValue = height(px, context);
    return EdgeInsets.symmetric(vertical: responsiveValue);
  }

  // Calculate responsive horizontal padding/margin
  static EdgeInsetsGeometry symmetric(BuildContext context,
      {double? horizontal, double? vertical}) {
    return EdgeInsets.symmetric(
        horizontal: horizontal != null ? width(horizontal, context) : 0.0,
        vertical: vertical != null ? height(vertical, context) : 0.0);
  }

  // Calculate responsive padding/margin for specific sides
  static EdgeInsetsGeometry fromLTRB(
      double left,
      double top,
      double right,
      double bottom,
      BuildContext context,
      ) {
    return EdgeInsets.fromLTRB(
      width(left, context),
      height(top, context),
      width(right, context),
      height(bottom, context),
    );
  } // Calculate responsive padding/margin for specific sides

  static EdgeInsetsGeometry only(
      BuildContext context, {
        double left = 0.0,
        double top = 0.0,
        double right = 0.0,
        double bottom = 0.0,
      }) {
    return EdgeInsets.only(
      left: width(left, context),
      top: height(top, context),
      right: width(right, context),
      bottom: height(bottom, context),
    );
  }

  static EdgeInsetsGeometry topPaddingOfSafeArea(
      BuildContext context, {
        double top = 0.0,
        double bottom = 0.0,
        double right = 0.0,
        double left = 0.0,
      }) {
    double figmaHeight = figmaSafeAreaHeight + top;
    double statusHeight = statusBarHeight(context);
    double inDifference = statusHeight > figmaHeight
        ? statusHeight - figmaHeight
        : figmaHeight - statusHeight;
    return EdgeInsets.only(
      top: inDifference,
      bottom: height(bottom, context),
      left: width(left, context),
      right: width(right, context),
    );
  }

  static double appBarHeight(
      BuildContext context, {
        double heightValue = 52.0,
      }) {
    double figmaHeight = figmaSafeAreaHeight + heightValue;
    double statusHeight = statusBarHeight(context);
    double inDifference = statusHeight > figmaHeight
        ? statusHeight - figmaHeight
        : figmaHeight - statusHeight;

    return inDifference;
  }
}

