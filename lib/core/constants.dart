import 'package:denie/app/modules/home/boarding_model.dart';
import 'package:denie/core/image_assets.dart';
import 'package:denie/core/responsive.dart';
import 'package:denie/core/size_config.dart';
import 'package:get/get.dart';

List<ContentData> contents(context) => [
  ContentData(
    mainTitle: 'Drei einfache Schritte\nzu deinem neuen Job',
    image1: ImageData(
      image: board1Svg,
      height: Responsive.height(ResponsiveScreenType.isDesktop(context) ?
      253: 145, context),
      width: Responsive.width(
          ResponsiveScreenType.isDesktop(context) ?  384: 220, context),
    ),
    text1: "Erstellen dein Lebenslauf",
    image2: ImageData(
      image: board2Svg,
      height: Responsive.height(
          ResponsiveScreenType.isDesktop(context) ? 227 : 127, context),
      width: Responsive.width(ResponsiveScreenType.isDesktop(context) ? 324:181, context),
    ),
    text2: "Erstellen dein Lebenslauf",
    image3: ImageData(
      image: board3Svg,
      height: Responsive.height( ResponsiveScreenType.isDesktop(context) ? 376 : 210, context),
      width: Responsive.width( ResponsiveScreenType.isDesktop(context) ? 502: 281, context),
    ),
    text3: "Mit nur einem Klick bewerben",
  ),
  ContentData(
    mainTitle: 'Drei einfache Schritte\nzu deinem neuen Mitarbeiter',
    image1: ImageData(
      image: board1Svg,
      height: Responsive.height(ResponsiveScreenType.isDesktop(context) ?
      253: 145, context),
      width: Responsive.width(
          ResponsiveScreenType.isDesktop(context) ?  384: 220, context),
    ),
    text1: "Erstellen dein Unternehmensprofil",
    image2: ImageData(
      image: board4Svg,
      height: Responsive.height(179, context),
      width: Responsive.width(259, context),
    ),
    text2: "Erstellen ein Jobinserat",
    image3: ImageData(
      image: board5Svg,
      height: Responsive.height(197, context),
      width: Responsive.width(240, context),
    ),
    text3: "Wähle deinen neuen Mitarbeiter aus",
  ),
  ContentData(
    mainTitle: 'Drei einfache Schritte zur\nVermittlung neuer Mitarbeiter',
    image1: ImageData(
      image: board1Svg,
      height: Responsive.height(ResponsiveScreenType.isDesktop(context) ?
      253: 145, context),
      width: Responsive.width(
          ResponsiveScreenType.isDesktop(context) ?  384: 220, context),
    ),
    text1: "Erstellen dein Unternehmensprofil",
    image2: ImageData(
      image: board6Svg,
      height: Responsive.height(148, context),
      width: Responsive.width(218, context),
    ),
    text2: "Erhalte Vermittlungs- angebot von Arbeitgeber",
    image3: ImageData(
      image: board7Svg,
      height: Responsive.height(190, context),
      width: Responsive.width(250, context),
    ),
    text3: "Vermittlung nach Provision oder Stundenlohn",
  ),
];
