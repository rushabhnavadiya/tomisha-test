
import 'package:denie/core/size_config.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

TextStyle latoRegular130(BuildContext context) => GoogleFonts.lato(
  fontSize: Responsive.fontSize(130,context),
  color: Color(0xFF718096),
);

TextStyle latoMedium21(BuildContext context) => GoogleFonts.lato(
  fontSize: Responsive.fontSize(21,context),
  color: Color(0xFF4A5568),
);

TextStyle latoRegular1575(BuildContext context) => GoogleFonts.lato(
  fontSize: Responsive.fontSize(16,context),
  color: Color(0xFF718096),
);

TextStyle latoRegular30(BuildContext context) => GoogleFonts.lato(
  fontSize: Responsive.fontSize(30,context),
  color: Color(0xFF718096),
);

TextStyle latoMedium42(BuildContext context) => GoogleFonts.lato(
  fontSize: Responsive.fontSize(42,context),
  color: Color(0xFF2D3748),
);

TextStyle latoMedium65(BuildContext context) => GoogleFonts.lato(
  fontSize: Responsive.fontSize(65,context),
  color: Color(0xFF2D3748),
  fontWeight:FontWeight.bold,
);

TextStyle latoBold14(BuildContext context) => GoogleFonts.lato(
  fontSize: Responsive.fontSize(14,context),
  fontWeight: FontWeight.bold,
  color: Color(0xFF319795),
);

TextStyle latoSemibold14(BuildContext context) => GoogleFonts.lato(
  fontSize: Responsive.fontSize(14,context),
  fontWeight: FontWeight.w600,
  color: Color(0xFF319795),
);

TextStyle latoNormal19(BuildContext context) => GoogleFonts.lato(
  fontSize: Responsive.fontSize(19,context),
  fontWeight: FontWeight.w600,
  color: Color(0xFF4A5568),
);